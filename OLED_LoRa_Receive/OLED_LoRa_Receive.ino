/* For use with LilyGo LORA-32 boards with OLED display
https://www.lilygo.cc/products/lora3
*/

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <LoRa.h>
#include <SPI.h>
#include <Wire.h>

#define SCK 5   // GPIO5  -- SX1278's SCK
#define MISO 19 // GPIO19 -- SX1278's MISnO
#define MOSI 27 // GPIO27 -- SX1278's MOSI
#define SS 18   // GPIO18 -- SX1278's CS
#define RST 14  // GPIO14 -- SX1278's RESET
#define DI0 26  // GPIO26 -- SX1278's IRQ(Interrupt Request)
#define BAND 915E6

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET -1 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS \
  0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

const uint8_t header[] = {0x61, 0x79, 0x79, 0x79, 0x79};
const uint8_t fingerprint[] = {0x6c, 0x6d, 0x61, 0x6f};

uint8_t packetsRecieved = 0;
uint8_t payloadSize = 0;
bool nextPacketIsPayloadSize = false;
bool gotHeader = false;

bool checkCode(uint8_t *buffer, const uint8_t *code)
{
  for (int i = 0; i < sizeof(code); i++)
  {
    if (buffer[i] != code[i])
    {
      return false;
    }
  }
  return true;
}

// Interface for the Python serial receiver
void cbk(int packetSize, bool recordPayloadSize)
{
  uint8_t packet_buffer[packetSize];
  for (int i = 0; i < packetSize; i++)
  {
    packet_buffer[i] = LoRa.read();
    Serial.printf("%02X", packet_buffer[i]);
    if (recordPayloadSize)
    {
      payloadSize = packet_buffer[i];
      nextPacketIsPayloadSize = false;
    }
  } 
  Serial.println();
  if(gotHeader){
    upperMessage("Rx Node");
    lowerMessage("Packet " + String(packetsRecieved) + "/" + String(payloadSize) + "        ");
    packetsRecieved++;
  }
  if (checkCode(packet_buffer, header))
  {
    upperMessage("Rx Node");
    nextPacketIsPayloadSize = true;
    gotHeader = true;
  }
  if (checkCode(packet_buffer, fingerprint) && gotHeader)
  {
    display.clearDisplay();
    upperMessage("Rx Node");
    lowerMessage("Download complete");
    packetsRecieved = 0;
    gotHeader = false;
  }
}

void setup()
{
  Serial.begin(38400);
  Serial.println("LoRa Receiver Callback");
  SPI.begin(SCK, MISO, MOSI, SS);
  LoRa.setPins(SS, RST, DI0);
  if (!LoRa.begin(BAND))
  {
    Serial.println("Starting LoRa failed!");
    while (1)
      ;
  }
  // LoRa.onReceive(cbk);
  LoRa.receive();
  Serial.println("init ok");

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS))
  {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ; // Don't proceed, loop forever
  }

  // Show initial display buffer contents on the screen --
  // the library initializes this with an Adafruit splash screen.
  display.display();
  delay(2000); // Pause for 2 seconds

  // Clear the buffer
  display.clearDisplay();

  display.setCursor(0, 0);
  display.setTextSize(2);
  display.setTextColor(SSD1306_WHITE);
  display.println("Rx Node");
  display.setCursor(0, 20);
  display.setTextSize(1);
  display.println("Waiting for data...");
  display.display();
  delay(1000);
}

void upperMessage(String s)
{
  display.setCursor(0, 0);
  display.setTextSize(2); 
  display.setTextColor(SSD1306_WHITE);
  display.print(s);
  display.display();
  delay(10);
}

void lowerMessage(String s)
{
  display.setCursor(0, 20);
  // display.setTextSize(2);
  display.setTextColor(SSD1306_WHITE);
  display.setTextColor(WHITE, BLACK);
  //display.display();
  //delay(10);
  display.setTextSize(1);
  //display.setCursor(0, 20);
  display.print(s);
  display.display();
  delay(10);
}
const uint16_t dataSize = 1385;
uint8_t imageData[dataSize];
uint dataIndex = 0;
void loop()
{
  delay(10);

  int packetSize = LoRa.parsePacket();
  if (packetSize)
  {
    cbk(packetSize, nextPacketIsPayloadSize);
  }
  // else
  // {
  //   upperMessage("Rx Node");
  //   lowerMessage("Waiting for data...");
  // }
}
