import serial
import cv2
import numpy as np
import binascii
import tqdm
from tqdm import TqdmWarning
import warnings
import time
warnings.filterwarnings("ignore", category=TqdmWarning)
def prep_data(databuf):
    data_string = b''.join(databuf).decode('utf-8')
    data = bytes.fromhex(data_string)
    image_np = np.frombuffer(data, np.uint8)
    image = cv2.imdecode(image_np, cv2.IMREAD_COLOR)
    return image

dev = '/dev/ttyACM0'
ser = serial.Serial(dev, 38400)
print("Connected to", dev)
print("Waiting for data header")
databuf = []
header =  binascii.hexlify(b'ayyyy')
fingerprint =b'6C6D616F' #binascii.hexlify(b'lmao')  # Header works fine but this doesn't, why?
while True:
    data = ser.readline()
    if data and header in data.strip():
        print("Got header")
        databuf = []
        payloadSizeNext = True
        break
while True:
    data = ser.readline()
    if payloadSizeNext and data:
        payloadSize = int(data.strip(),16)
        increment =  100 / payloadSize
        payloadSizeNext = False
        pbar = tqdm.tqdm(total=100, desc="Downloading Payload", unit="%", unit_scale=True)
        continue
    if fingerprint in data.strip():
        pbar.update(increment)
        break
    if data:
        pbar.update(increment)
        databuf.append(data.strip())



ser.close()
image = prep_data(databuf)
cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
cv2.imshow("Image", image)
cv2.waitKey(0)