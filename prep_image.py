import cv2
import sys

img = cv2.imread(sys.argv[1])
# Conver the image to bytes
img_data = cv2.imencode('.jpg', img)[1].tobytes()
# Print the bytes to the console
for i, byte in enumerate(img_data):
    if i % 64 == 0 and i != 0:
        print()
    print(f"{hex(byte)},", end="")