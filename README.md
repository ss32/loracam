# loracam


Code and helper scripts for sending and receiving imagery over LoRa.  

## Workflow

1. Prepare the image with `python3 prep_image.py /path/to/image.jpg`
2. Copy the output and paste it into the `imageData` array in `OLED_LoRa_Sender.ino`
3. With both the transmit board transmitting, plut the receiver into your computer.
4. Change the `dev` string in `monitor_serial.py` to match the device of your receiver board (ex: `/dev/ttyACM0` on Linux or `COM3` on Windows)
5. Run `monitor_serial.py` and wait for packets to be recieved.

![demo](demo.jpg)